<?php
/*-------------------------------------------------------*/
/* Run OBX Media framework (required)
/*-------------------------------------------------------*/

require_once( get_template_directory() . '/framework/themeblvd.php' );

/*-------------------------------------------------------*/
/* Start Child Theme
/*-------------------------------------------------------*/
// remove version info from head and feeds
function complete_version_removal() {
	return '';
}
add_filter('the_generator', 'complete_version_removal');
//remove themeblvd debug info
function my_remove_debug() {
    if ( class_exists('Theme_Blvd_Frontend_Init') ) {
        $init = Theme_Blvd_Frontend_Init::get_instance();
        remove_action('wp_head', array($init, 'debug'));
    }
}
add_action('wp', 'my_remove_debug');
## Function to remove emoji scripts
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
// remove the query strings
function _remove_script_version( $src ){
	$parts = explode( '?ver', $src );
        return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

// remove dashboard widgets
function remove_dashboard_widgets(){
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal');   // Plugins
    remove_meta_box('dashboard_secondary', 'dashboard', 'side');   // Other WordPress News
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets');

// customize admin footer text
function custom_admin_footer() {
	echo 'Website Design and Development by <a href="http://www.outerbanksmedia.com/" title="Outer Banks Media Internet Marketing" target="_blank">Outer Banks Media</a>';
} 
add_filter('admin_footer_text', 'custom_admin_footer');

// modify the login logo and link
add_filter( 'login_headerurl', 'namespace_login_headerurl' );
/**
 * Replaces the login header logo URL
 *
 * @param $url
 */
function namespace_login_headerurl( $url ) {
    $url = home_url( '/' );
    return $url;
}

add_filter( 'login_headertitle', 'namespace_login_headertitle' );
/**
 * Replaces the login header logo title
 *
 * @param $title
 */
function namespace_login_headertitle( $title ) {
    $title = get_bloginfo( 'name' );
    return $title;
}

add_action( 'login_head', 'namespace_login_style' );
/**
 * Replaces the login header logo
 */
function namespace_login_style() {
    echo '<style>#login {padding:0 !important} .login h1 a { background-image: url( /img/logo-admin.png ) !important; background-size: 320px 183px;height:183px;width:320px }</style>';
}

//SEO by Yoast rewrite title tag
function my_title() {
	wp_title('');
}
remove_action( 'themeblvd_title', 'themeblvd_title_default' );
add_action( 'themeblvd_title', 'my_title' );

//SECURITY MODS
define ( 'DISALLOW_FILE_EDIT', true );

// Contact form Event Tracking
add_action("gform_after_submission_11", "contact_form_tracking", 10, 2);

function contact_form_tracking($entry, $form) { 

  ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-XXXXXXXX-1', 'reliant-nc.com');

</script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
			ga('send', 'pageview', '/thank-you/');
			ga('send', 'event', 'lead-form', '<?php echo $form["title"]; ?>', 'page: <?php echo $entry["source_url"]; ?>');
        });
    </script>

<?php }

// do shortcodes
add_filter('widget_text', 'do_shortcode');
add_filter('the_excerpt', 'do_shortcode');
// [likebox]
function likebox_here() {
	return '<div class="fb-page" data-href="https://www.facebook.com/StanWhiteRealty" data-width="500px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/StanWhiteRealty"><a href="https://www.facebook.com/StanWhiteRealty">Stan White Realty</a></blockquote></div></div>';
}
add_shortcode( 'likebox', 'likebox_here' );
// [likebutton]
function likebutton_here() {
	return '<div class="fb-like" data-href="http://www.swobx.com" data-width="auto" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>';
}
add_shortcode( 'likebutton', 'likebutton_here' );
// [socialite]
function ck_socialite() {
	return'<?php wpsocialite_markup(); ?>';
}
add_shortcode( 'socialite', 'ck_socialite' );
// Custom FB Code
function add_ck_customs() {
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=1038918739452276";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php
}
add_action('themeblvd_before', 'add_ck_customs',99);